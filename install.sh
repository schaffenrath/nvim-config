#!/bin/bash

if [ "$(id -u)" -eq 0 ]; then
    echo "Don't run this script as root. Will call sudo for neccessary steps!"
    exit 1
fi

# Install nvim
if ! command -v nvim &>/dev/null; then
    echo "Install latest neovim"
    wget https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
    tar -xzvf nvim-linux64.tar.gz
    cd nvim-linux64 && sudo cp -r ./bin/* /usr/bin/ && sudo cp -r ./lib/* /usr/lib/ && sudo cp -r ./share/* /usr/share/
    # Back to install dir and remove install files
    cd ..
    rm -rf nvim-linux64.tar.gz nvim-linux64
else
    if [[ -z $(nvim --version | grep "v0.1") ]]; then
        echo "Too old neovim version installed. Please uninstall to proceed!"
        exit 1
    else
        echo "Neovim >= v0.10 already installed"
    fi
fi

# Install dependencies
sudo apt-get update && apt-get install -y python3-pynvim && apt-get install -y python3-venv
sudo apt-get install -y ripgrep && apt-get install -y fd-find && apt-get install -y luarocks
sudo apt-get install -y clang-format && apt-get install -y codespell

# Install nodejs for language servers
if ! command -v node &>/dev/null; then
    echo "Install NodeJS"
    # installs fnm (Fast Node Manager)
    curl -fsSL https://fnm.vercel.app/install | bash
    source "$HOME/.bashrc"
    # download and install Node.js
    fnm use --install-if-missing 20
fi

# Install stylua for better lua support (editing config files)
if command -v cargo &>/dev/null; then
    cargo install stylua
else
    echo "Rust is not installed. Cannot install stylua!"
fi

# Install Nvim configuration
cp -r ./nvim "$HOME/.config/"

echo "Finished install"
echo "Run 'nvim' to start package manager"
