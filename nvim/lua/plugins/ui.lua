-- Plugins to enhance UI
return {

	-- Bottom bar for file insights
	{
		"nvim-lualine/lualine.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		opts = {},
	},

	-- Nice input masks for nvim
	{
		"stevearc/dressing.nvim",
	},

	-- Fancy icons
	{
		"echasnovski/mini.icons",
		version = false,
		opts = {},
	},

	-- Show keymaps in UI
	{
		"folke/which-key.nvim",
		event = "VeryLazy",
		opts = {
			-- your configuration comes here
			-- or leave it empty to use the default settings
			-- refer to the configuration section below
		},
		keys = {
			{
				"<leader>?",
				function()
					require("which-key").show({ global = false })
				end,
				desc = "Buffer Local Keymaps (which-key)",
			},
		},
	},
	-- Popup windows for searching files, strings, commands,...
	{
		"nvim-telescope/telescope.nvim",
		tag = "0.1.8",
		dependencies = {
			{ "nvim-lua/plenary.nvim" },
			{ "nvim-telescope/telescope-live-grep-args.nvim", version = "^1.0.0" },
		},
		keys = {
			{ "<leader>:", "<cmd>Telescope commands<CR>", desc = "Command" },
			{
				"<leader>fg",
				":lua require('telescope').extensions.live_grep_args.live_grep_args()",
				desc = "Grep string",
			},
			{ "<leader>gc", "<cmd>Telescope git_commits<CR>", desc = "Commit" },
			{ "<leader>gi", "<cmd>Telescope git_status<CR>", desc = "Status" },
			{
				"<leader>fs",
				function()
					require("telescope.builtin").lsp_document_symbols({
						symbols = LazyVim.config.get_kind_filter(),
					})
				end,
				desc = "Goto Symbol",
			},
		},
		opts = function()
			local function find_command()
				if 1 == vim.fn.executable("rg") then
					return { "rg", "--files", "--color", "never", "-g", "!.git" }
				elseif 1 == vim.fn.executable("fd") then
					return { "fd", "--type", "f", "--color", "never", "-E", ".git" }
				elseif 1 == vim.fn.executable("fdfind") then
					return { "fdfind", "--type", "f", "--color", "never", "-E", ".git" }
				elseif 1 == vim.fn.executable("find") and vim.fn.has("win32") == 0 then
					return { "find", ".", "-type", "f" }
				elseif 1 == vim.fn.executable("where") then
					return { "where", "/r", ".", "*" }
				end
			end
			return {
				pickers = {
					find_files = {
						find_command = find_command,
						hidden = true,
					},
				},
			}
		end,
	},
}
