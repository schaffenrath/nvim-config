require("tiny-inline-diagnostic").setup({
	options = {
		multiple_diag_under_cursor = true,
		multilines = true,
		show_all_diags_on_cursorline = true,
	},
})
