local dap, dapui = require("dap"), require("dapui")
dap.listeners.before.attach.dapui_config = function()
	dapui.open()
end
dap.listeners.before.launch.dapui_config = function()
	dapui.open()
end
dap.listeners.before.event_terminated.dapui_config = function()
	dapui.close()
end
dap.listeners.before.event_exited.dapui_config = function()
	dapui.close()
end

vim.keymap.set("n", "<leader>dw", function()
	dapui.toggle()
end, { desc = "Open Debug window" })

vim.keymap.set("n", "<leader>di", function()
	dapui.eval()
end, { desc = "Evaluate expression" })

local ui = require("dapui")
ui.setup({
	icons = { expanded = "▾", collapsed = "▸" },
	mappings = {
		open = "o",
		remove = "d",
		edit = "e",
		repl = "r",
		toggle = "t",
	},
	expand_lines = vim.fn.has("nvim-0.7"),
	floating = {
		max_height = nil,
		max_width = nil,
		border = "single",
		mappings = {
			close = { "q", "<Esc>" },
		},
	},
	windows = { indent = 1 },
	render = {
		max_type_length = nil,
	},
})

vim.fn.sign_define("DapBreakpoint", { text = "🟥" })

vim.keymap.set("n", "<F5>", function()
	dap.continue()
end, { desc = "Continue" })
vim.keymap.set("n", "<F10>", function()
	dap.step_over()
end, { desc = "Step Over" })
vim.keymap.set("n", "<F11>", function()
	dap.step_into()
end, { desc = "Step Into" })
vim.keymap.set("n", "<F12>", function()
	dap.step_out()
end, { desc = "Step Out" })

vim.keymap.set("n", "<leader>db", function()
	dap.toggle_breakpoint()
end, { desc = "Toggle Breakpoint" })
