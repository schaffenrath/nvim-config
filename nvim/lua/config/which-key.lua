local wk = require("which-key")
wk.add({
    { "<leader>u", "Undo-tree", icon = "󰦓", mode = "n" },
    { "<leader>y", "Clippboard yank", icon = "󰆼", mode = "n" },
    { "<leader>w", group = "Window", icon = "󰟀", mode = "n" },
    { "<leader>c", group = "Code", mode = "n" },
    { "<leader>cd", "<cmd>w !diff % - <CR>", desc = "Buffer diff", mode = "n", icon = "󰦓" },
    { "<leader>ct", "<cmd>TodoTelescope<CR>", desc = "Show TODOs", mode = "n", icon = "󰭎" },

    { "<leader>d", group = "Debug", mode = "n" },

    { "<leader>f", group = "Find", mode = "n" },
    { "<leader>ft", "<cmd>NvimTreeToggle<cr>", desc = "FileTree", mode = "n" },
    { "<leader>ff", "<cmd>Telescope find_files<cr>", desc = "File", mode = "n" },
    { "<leader>fb", "<cmd>Telescope buffers<cr>", desc = "Buffer", mode = "n" },
    { "<leader>fg", "<cmd>Telescope live_grep<cr>", desc = "Grep String", mode = "n" },
    { "<leader>fh", "<cmd>Telescope help<cr>", desc = "Help", mode = "n" },

    { "<leader>h", group = "Harpoon", icon = "󱡀", mode = "n" },

    { "<leader>s", group = "Surround", icon = "󰘦", mode = "n" },
    { "<leader>sa", "<cmd>sa<cr>", desc = "Add surround", mode = "n" },
    { "<leader>sd", "<cmd>sa<cr>", desc = "Remove surround", mode = "n" },

    { "<leader>x", group = "Trouple", icon = "󱌢", mode = "n" },
})
