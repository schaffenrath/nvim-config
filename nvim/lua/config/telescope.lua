require("telescope").setup({
	extensions = {
		["ui-select"] = {
			require("telescope.themes").get_dropdown(),
		},
	},
})

pcall(require("telescope").load_extension, "fzf")
pcall(require("telescope").load_extension, "ui-select")
pcall(require("telescope").load_extension, "live_grep_args")

local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>ff", builtin.find_files, { desc = "Files" })
vim.keymap.set("n", "<leader>fv", builtin.commands, { desc = "Vim Commands" })
vim.keymap.set("n", "<leader>fg", builtin.live_grep, { desc = "Grep in Dir" })
vim.keymap.set("n", "<leader>fb", builtin.buffers, { desc = "Buffers" })
vim.keymap.set("n", "<leader>fi", builtin.git_files, { desc = "Files in Git" })
vim.keymap.set("n", "<leader>fh", builtin.help_tags, { desc = "Help tags" })
vim.keymap.set("n", "<leader>fo", builtin.oldfiles, { desc = "Opened files" })
vim.keymap.set("n", "<leader>fs", builtin.treesitter, { desc = "String grep" })
vim.keymap.set("n", "<leader>fc", builtin.git_commits, { desc = "Commits" })
vim.keymap.set("n", "<leader>fw", function()
	builtin.live_grep({
		grep_open_files = true,
		prompt_title = "String in open Files",
	})
end, { desc = "Word" })
