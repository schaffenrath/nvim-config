local harpoon = require("harpoon")

-- REQUIRED
harpoon:setup()
-- REQUIRED

vim.keymap.set("n", "<leader>hi", function()
	harpoon:list():add()
end, { desc = "Add file" })
vim.keymap.set("n", "<C-h>", function()
	harpoon.ui:toggle_quick_menu(harpoon:list())
end, { desc = "Opened Files" })

vim.keymap.set("n", "<leader>ha", function()
	harpoon:list():select(1)
end, { desc = "File 1" })
vim.keymap.set("n", "<leader>hs", function()
	harpoon:list():select(2)
end, { desc = "File 2" })
vim.keymap.set("n", "<leader>hd", function()
	harpoon:list():select(3)
end, { desc = "File 3" })
vim.keymap.set("n", "<leader>hf", function()
	harpoon:list():select(4)
end, { desc = "File 4" })

-- Toggle previous & next buffers stored within Harpoon list
vim.keymap.set("n", "<leader>hp", function()
	harpoon:list():prev()
end, { desc = "Previous File" })
vim.keymap.set("n", "<leader>hn", function()
	harpoon:list():next()
end, { desc = "Next File" })
