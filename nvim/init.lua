require("config.lazy")
require("config.theme")
require("config.oil")
require("config.dap")
require("config.telescope")
require("config.harpoon")
require("config.which-key")
require("config.dressing")
require("config.tiny-inline-diagnostic")

vim.g.mapleader = " "
vim.wo.number = true
vim.wo.relativenumber = true

vim.o.expandtab = true
vim.o.tabstop = 4
vim.o.shiftwidth = 4

vim.o.listchars = "tab:>·,trail:~,extends:>,precedes:<,space:␣"
vim.o.list = true

-- Changing lines
vim.api.nvim_set_keymap("n", "<C-j>", "ddp", { desc = "Move line down" })
vim.api.nvim_set_keymap("n", "<C-k>", "ddkp", { desc = "Move line up" })
-- Clipboard yanking
vim.api.nvim_set_keymap("n", "<leader>y", '"+yy', { desc = "Yank line to clipboard" })
vim.api.nvim_set_keymap("v", "<leader>y", '"+y', { desc = "Yank selection to clipboard" })

-- Show changes in buffer
vim.api.nvim_set_keymap("n", "<leader>df", ":w !diff % -<CR>", { desc = "Display changed in buffer" })

-- Only show multline diagnostics when cursor hovers
vim.diagnostic.config({ virtual_lines = { only_current_line = false }, virtual_text = false })

-- Quick navigation through buffers and tabs
vim.keymap.set({ "v", "n" }, "]t", "<cmd>tabnext<CR>", { desc = "tab next" })
vim.keymap.set({ "v", "n" }, "]T", "<cmd>tabLast<CR>", { desc = "tab last" })
vim.keymap.set({ "v", "n" }, "[t", "<cmd>tabprevious<CR>", { desc = "tab last" })
vim.keymap.set({ "v", "n" }, "[T", "<cmd>tabFirst<CR>", { desc = "tab last" })
vim.keymap.set({ "v", "n" }, "]b", "<cmd>bnext<CR>", { desc = "buffer next" })
vim.keymap.set({ "v", "n" }, "]B", "<cmd>blast<CR>", { desc = "buffer last" })
vim.keymap.set({ "v", "n" }, "[b", "<cmd>bprevious<CR>", { desc = "buffer last" })
vim.keymap.set({ "v", "n" }, "[B", "<cmd>bfirst<CR>", { desc = "buffer last" })

-- Highlight when yanking (copying) text
--  See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd("TextYankPost", {
	desc = "Highlight when yanking (copying) text",
	group = vim.api.nvim_create_augroup("highlight-yank", { clear = true }),
	callback = function()
		vim.highlight.on_yank()
	end,
})
