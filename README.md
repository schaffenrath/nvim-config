# Neovim configuration

This project involves an installation script for an initial setup of neovim, as well as the basic configuration to start development quickly.

## Plugins

lazy.nvim is used as the package manager. All packages are described in files within the "plugins" directory. Configurations for the plugins are either located in the install file within "plugins" or separated in a lua file in the "config" directory.
Multiple restarts might be required to install and configure all packages.

## Notes

1. There might be conflicting keymaps in the current configuration.
2. Installing language support is done through the "Mason" plugin. Can be invoked through ":Mason" or "<space>:Mason".
3. Dependencies have not a fixed version yet. Might introduce breaking changes in the future. Should be fixed in the future.
