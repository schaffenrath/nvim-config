from ranger.api.commands import Command


class fzf_select(Command):
    """
    :fzf_select
    Find a file using fzf.
    With a prefix argument to select only directories.

    See: https://github.com/junegunn/fzf
    """

    def execute(self):
        import subprocess
        import os
        from ranger.ext.get_executables import get_executables

        if "fzf" not in get_executables():
            self.fm.notify("Could not find fzf in the PATH.", bad=True)
            return

        fd = None
        if "fdfind" in get_executables():
            fd = "fdfind"
        elif "fd" in get_executables():
            fd = "fd"

        if fd is not None:
            hidden = "--hidden" if self.fm.settings.show_hidden else ""
            exclude = "--no-ignore-vcs --exclude '.git' --exclude '*.py[co]' --exclude '__pycache__'"
            only_directories = "--type directory" if self.quantifier else ""
            fzf_default_command = "{} --follow {} {} {} --color=always".format(fd, hidden, exclude, only_directories)
        else:
            hidden = "-false" if self.fm.settings.show_hidden else r"-path '*/\.*' -prune"
            exclude = r"\( -name '\.git' -o -name '*.py[co]' -o -fstype 'dev' -o -fstype 'proc' \) -prune"
            only_directories = "-type d" if self.quantifier else ""
            fzf_default_command = "find -L . -mindepth 1 {} -o {} -o {} -print | cut -b3-".format(
                hidden, exclude, only_directories
            )

        env = os.environ.copy()
        env["FZF_DEFAULT_COMMAND"] = fzf_default_command
        env["FZF_DEFAULT_OPTS"] = '--layout=reverse --ansi --preview="bat --color=always {1} | head -n 100" \
            --preview-window "right,60%,border-bottom" --bind "enter:execute(nvim {1})" \
            --bind "ctrl-d:preview-page-down" --bind "ctrl-u:preview-page-up" \
            --bind "ctrl-e:preview-down" --bind "ctrl-y:preview-up"'

        fzf = self.fm.execute_command("fzf --no-multi", env=env, universal_newlines=True, stdout=subprocess.PIPE)
        stdout, _ = fzf.communicate()
        if fzf.returncode == 0:
            selected = os.path.abspath(stdout.strip())
            if os.path.isdir(selected):
                self.fm.cd(selected)
            else:
                self.fm.select_file(selected)


class fzf_rga(Command):
    """
    :fzf_rga_search_documents
    Search in PDFs, E-Books and Office documents in current directory.
    Allowed extensions: .epub, .odt, .docx, .fb2, .ipynb, .pdf.

    Usage: fzf_rga_search_documents <search string>
    """

    def execute(self):
        import subprocess
        import os.path
        from ranger.container.file import File

        # command = "rga '%s' . --rga-adapters=pandoc,poppler | fzf +m | awk -F':' '{print $1}'" % search_string
        env = os.environ.copy()
        env["RG_PREFIX"] = ""
        env["FZF_DEFAULT_OPTS"] = '--ansi --disabled \
            --bind "start:reload:rg --column --line-number --no-heading --color=always --smart-case {q}" \
            --bind "change:reload:sleep 0.1;rg --column --line-number --no-heading --color=always --smart-case {q} || true" \
            --delimiter : \
            --preview "bat --color=always {1} --highlight-line {2}" \
            --preview-window "right,60%,border-bottom,+{2}+3/3,~3" \
            --bind "enter:execute(nvim {1} +{2})" \
            --bind "ctrl-d:preview-page-down" --bind "ctrl-u:preview-page-up" \
            --bind "ctrl-e:preview-down" --bind "ctrl-y:preview-up" \
            '

        fzf = self.fm.execute_command("fzf --no-multi", env=env, universal_newlines=True, stdout=subprocess.PIPE)
        stdout, stderr = fzf.communicate()
        if fzf.returncode == 0:
            fzf_file = os.path.abspath(stdout.rstrip("\n"))
            self.fm.execute_file(File(fzf_file))
